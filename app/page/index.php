<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Main</title>
    <link rel="stylesheet" href="/css/index.css<?= VER ?>">
    <link rel="stylesheet" href="/css/font-awesome.min.css<?= VER ?>">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,800" rel="stylesheet">

    <script type="text/javascript" src="/js/jquery.min.js"></script>
</head>
<body id="body">


<div id="device"></div>

<div class="site">
    <!--шапка для всех страниц-->
    <div class="site__head">
        <div class="bigmenu">
            <div class="bigmenu__wrap">
                <div class="bigmenu__head">
                    <div class="-close"><i class="fa fa-times" aria-hidden="true"></i></div>
                    <a href="/"><img src="/img/logo.svg" class="img-responsive"></a>
                    <a href="/" class="-mini">
                        <img src="/img/logo-mini.svg" class="img-responsive">
                    </a>
                </div>
                <div class="bigmenu__body">
                    <nav>
                        <div><a href="/">
                                <small>
                                    <svg viewBox="0 0 21 19" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="1.414"><path d="M18.148 10.176v8.47H11.53v-7.331H8.611v7.33H1.993v-8.469l-.925.926L0 10.034 10.106-.001l10.035 10.035-1.068 1.068-.925-.926zm-5.124 7.046h3.7v-8.54l-6.618-6.619-6.619 6.619v8.54h3.63V9.82h5.907v7.402z" fill="#fff" fill-opacity=".5"/></svg>
                                </small>
                                <span>Главная</span></a></div>
                        <div><a href="/"><small><svg viewBox="0 0 18 25" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="1.414"><path d="M7.024 0h8.264a2.078 2.078 0 0 1 2.066 2.066v14.875a2.078 2.078 0 0 1-2.066 2.066H11.57v1.088c-.003 2.576-2.12 4.694-4.696 4.697H.413A.415.415 0 0 1 0 24.379V13.846a4.563 4.563 0 0 1 .996-2.84l3.962-4.953V2.066A2.078 2.078 0 0 1 7.024 0zm0 .827c-.68 0-1.24.56-1.24 1.24v10.837l1.653-1.757V.827h-.413zm1.24 9.442l.707-.75a1.92 1.92 0 0 1 .532-.391V.827h-1.24v9.442zm8.264 6.672V2.066c0-.68-.56-1.24-1.24-1.24H10.33v8.092h.004a1.835 1.835 0 0 1 1.38.56 1.93 1.93 0 0 1 .107 2.584l-1.491 1.763v4.356h4.958c.68 0 1.24-.56 1.24-1.24zm-8.305 1.24h1.28V14.8l-1.24 1.465v1.277a5.398 5.398 0 0 1-.04.638zm-6.581-6.658a3.728 3.728 0 0 0-.816 2.323v10.12h6.047a3.893 3.893 0 0 0 3.87-3.87v-1.089H8.059A5.54 5.54 0 0 1 4.691 22.7l-.29-.774a4.71 4.71 0 0 0 3.036-4.383v-1.428c0-.098.035-.192.098-.267l3.655-4.32c.36-.429.333-1.068-.06-1.467a1.047 1.047 0 0 0-.786-.317 1.075 1.075 0 0 0-.773.34l-5.553 5.901-.602-.567 1.542-1.636V7.377l-3.316 4.146z" fill="#fff" fill-opacity=".5" fill-rule="nonzero"/></svg></small><span>Платежи</span></a></div>
                        <div><a href="/"><small><svg viewBox="0 0 22 20" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="1.414"><path d="M1.916 9.578c0-2.56.996-4.965 2.805-6.773A9.515 9.515 0 0 1 11.494 0c2.558 0 4.964.996 6.773 2.805a9.514 9.514 0 0 1 2.806 6.773 9.515 9.515 0 0 1-2.805 6.773 9.549 9.549 0 0 1-6.773 2.8 9.547 9.547 0 0 1-6.773-2.8.957.957 0 1 1 1.355-1.355c2.987 2.988 7.848 2.988 10.836 0a7.612 7.612 0 0 0 2.244-5.418 7.612 7.612 0 0 0-2.244-5.419 7.613 7.613 0 0 0-5.419-2.244A7.613 7.613 0 0 0 6.076 4.16 7.612 7.612 0 0 0 3.83 9.578h1.916l-2.873 3.831L0 9.578h1.916zm8.62-5.747a.958.958 0 0 0-.958.958v5.747c0 .53.43.958.958.958h3.832a.958.958 0 1 0 0-1.916h-2.874v-4.79c0-.529-.43-.957-.958-.957z" fill="#fff" fill-opacity=".5" fill-rule="nonzero"/></svg></small><span>События</span></a></div>
                        <div><a href="/"><small><svg viewBox="0 0 23 23" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="1.414"><g opacity=".5" fill="#fff" fill-rule="nonzero"><path d="M11.5 7.393c-2.253 0-4.107 1.854-4.107 4.107s1.854 4.107 4.107 4.107 4.107-1.854 4.107-4.107c-.002-2.252-1.855-4.105-4.107-4.107zm0 6.571A2.476 2.476 0 0 1 9.036 11.5 2.476 2.476 0 0 1 11.5 9.036a2.476 2.476 0 0 1 2.464 2.464 2.479 2.479 0 0 1-2.464 2.464z"/><path d="M22.34 9.052l-2.283-.458a8.565 8.565 0 0 0-.454-1.09l1.292-1.94a.823.823 0 0 0-.103-1.035l-2.32-2.321a.824.824 0 0 0-1.036-.103l-1.94 1.292a8.562 8.562 0 0 0-1.09-.454L13.949.66a.824.824 0 0 0-.805-.66H9.857a.824.824 0 0 0-.805.66l-.458 2.283a8.567 8.567 0 0 0-1.09.454l-1.94-1.292a.823.823 0 0 0-1.036.103l-2.32 2.32a.824.824 0 0 0-.103 1.036l1.292 1.94a8.586 8.586 0 0 0-.454 1.09L.66 9.051a.824.824 0 0 0-.66.805v3.286c0 .39.278.728.66.805l2.283.458c.126.374.278.738.454 1.09l-1.292 1.94a.824.824 0 0 0 .103 1.035l2.32 2.321a.823.823 0 0 0 1.036.103l1.94-1.292c.352.176.716.328 1.09.454l.458 2.283c.076.382.415.66.805.66h3.286c.39 0 .728-.278.805-.66l.458-2.283a8.506 8.506 0 0 0 1.09-.454l1.94 1.292a.824.824 0 0 0 1.035-.103l2.32-2.32a.823.823 0 0 0 .104-1.036l-1.292-1.94a8.5 8.5 0 0 0 .454-1.09l2.284-.458a.824.824 0 0 0 .659-.805V9.857a.824.824 0 0 0-.66-.805zm-.983 3.418l-2.104.422a.822.822 0 0 0-.631.59 6.9 6.9 0 0 1-.687 1.648.824.824 0 0 0 .028.866l1.191 1.79-1.368 1.368-1.79-1.191a.823.823 0 0 0-.866-.028 6.91 6.91 0 0 1-1.649.687.822.822 0 0 0-.59.631l-.421 2.104h-1.94l-.422-2.104a.822.822 0 0 0-.59-.631 6.91 6.91 0 0 1-1.648-.687.822.822 0 0 0-.867.028l-1.789 1.191-1.368-1.368 1.191-1.79a.823.823 0 0 0 .027-.866 6.915 6.915 0 0 1-.686-1.648.822.822 0 0 0-.63-.59l-2.105-.422v-1.94l2.104-.422a.822.822 0 0 0 .631-.59 6.9 6.9 0 0 1 .686-1.648.824.824 0 0 0-.027-.867L3.845 5.214l1.369-1.369 1.79 1.192c.259.173.596.184.866.027a6.91 6.91 0 0 1 1.649-.686.822.822 0 0 0 .589-.631l.422-2.104h1.94l.422 2.104c.06.305.29.55.59.631a6.91 6.91 0 0 1 1.648.686c.27.157.606.146.866-.027l1.79-1.191 1.368 1.368-1.191 1.79a.823.823 0 0 0-.028.866c.3.518.53 1.072.687 1.648.081.3.326.529.63.59l2.105.422v1.94z"/></g></svg></small><span>Настройки</span></a></div>
                        <div><a href="/"><small><svg viewBox="0 0 22 22" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="1.414"><g opacity=".5"><path d="M13.465 5.965a2.705 2.705 0 0 0-2.692-2.678A2.706 2.706 0 0 0 8.08 5.979a2.706 2.706 0 0 0 2.693 2.693 2.715 2.715 0 0 0 2.692-2.707zm-4.411 0a1.727 1.727 0 0 1 1.719-1.705c.943 0 1.719.776 1.719 1.72 0 .942-.776 1.718-1.72 1.718a1.737 1.737 0 0 1-1.718-1.733z" fill="#fff" fill-rule="nonzero"/><path d="M1.467 12.748l-1.46 8.095a.49.49 0 0 0 .48.572h20.587a.49.49 0 0 0 .48-.572l-1.461-8.095a.489.489 0 0 0-.487-.402h-5.067l1.269-1.704a6.722 6.722 0 0 0 1.37-4.446C16.945 2.729 14.106 0 10.712 0S4.479 2.73 4.245 6.206a6.722 6.722 0 0 0 1.371 4.445l1.269 1.705H1.947a.488.488 0 0 0-.48.392zm3.75-6.479C5.392 3.671 7.496.974 10.712.974s5.32 2.678 5.495 5.29a5.748 5.748 0 0 1-1.18 3.798l-4.315 5.78L6.39 10.07A5.746 5.746 0 0 1 5.217 6.27V6.27zm2.396 7.06l2.71 3.618a.487.487 0 0 0 .78 0l2.708-3.625h5.38l1.286 7.129H1.068l1.286-7.121h5.259z" fill="#fff" fill-rule="nonzero"/></g></svg></small><span>Контакты</span></a></div>
                    </nav>
                </div>
                <div class="bigmenu__footer">
                    <a href="/" class="btn -red">Выход</a>
                </div>
            </div>
        </div>
    </div>
    <!--контент страниц-->
    <div class="site__mob">
        <div class="fx">
            <div class="-wrap">
                <div class="-logo">
                    <a href="/"><img src="/img/logo-mobile.svg" class="img-responsive"></a>
                </div>
                <div class="-right">
                <div class="-name"><a href="/">Котов И.Н.</a></div>
                <div class="-bar"><i class="fa fa-bars" aria-hidden="true"></i></div>
            </div>
            </div>
        </div>
    </div>
    <div class="site__body">
        <div class="content">
            <div class="content__body">
                <div class="dogovor">
                    <div class="fx">
                        <div class="dogovor__name">
                            <div class="-name">
                                <div class="-top">Котов И.Н.</div>
                                <span class="-bot"></span>
                            </div>
                            <div class="-number">
                                <div class="-top">78999039</div>
                                <span class="-bot">№ договора</span>
                            </div>
                            <div class="-date">
                                <div class="-top">01.05.2014</div>
                                <span class="-bot">Дата регистрации</span>
                            </div>
                        </div>
                        <div class="dogovor__add">
                            <div class="-price"><span>600</span>P</div>
                            <div class="-add"><a href="/" class="btn -red">Пополнить</a></div>
                            <div class="-kvit"><a href="/"><svg viewBox="0 0 21 15" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="1.414"><path d="M9.449 11.55h-1.05v-8.4h1.05v8.4zm8.4-8.4h-1.05v8.4h1.05v-8.4zm-10.5 0h-2.1v8.4h2.1v-8.4zm6.3 0h-3.15v8.4h3.15v-8.4zm-9.45 0h-1.05v8.4h1.05v-8.4zm16.8-2.1v12.6c0 .576-.474 1.05-1.05 1.05h-18.9c-.576 0-1.05-.474-1.05-1.05V1.05C-.001.474.473 0 1.049 0h18.9c.576 0 1.05.474 1.05 1.05zm-1.05 0h-18.9v12.6h18.9V1.05zm-4.2 2.1h-1.05v8.4h1.05v-8.4z" fill="#009bdf" fill-rule="nonzero"/></svg><span>Квитанция для оплаты</span></a></div>
                            <div class="-info">Оплата ежемесячных платежей (абонентская плата) осуществляется Абонентом в срок не позднее 10 числа месяца, следующего за месяцем предоставления Услуг</div>
                        </div>
                    </div>
                </div>
                <div class="dogovor-content">
                    <div class="fx">
                        <table class="dogovor-content__table" cellpadding="0" cellspacing="0">
                            <tr>
                                <th class="dogovor-content__num">№</th>
                                <th class="dogovor-content__name">Объект</th>
                                <th class="dogovor-content__plata">Абонентская плата</th>
                                <th class="dogovor-content__filter"></th>
                            </tr>
                            <tr>
                                <td class="dogovor-content__num">1</td>
                                <td class="dogovor-content__name">191186, Санкт-Петербург г, Невский пр-кт, дом № 18, кв. 20</td>
                                <td class="dogovor-content__plata"><span>0.00</span><small>Абонентская плата</small></td>
                                <td class="dogovor-content__filter">
                                    <button>
                                        <img src="/img/filter.png">
                                        <span>Управлять</span>
                                    </button>
                                </td>
                            </tr>
                            <tr>
                                <td class="dogovor-content__num">1</td>
                                <td class="dogovor-content__name">191186, Санкт-Петербург г, Невский пр-кт, дом № 18, кв. 20</td>
                                <td class="dogovor-content__plata"><span>0.00</span><small>Абонентская плата</small></td>
                                <td class="dogovor-content__filter">
                                    <button>
                                        <img src="/img/filter.png">
                                        <span>Управлять</span>
                                    </button>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="content__footer">
                <div class="copyright">
                    <div class="fx">© 2019 ООО «Росохрана Телеком»</div>
                </div>
            </div>
        </div>
    </div>
</div>




<script src="/js/app.js"></script>


</body>
</html>
