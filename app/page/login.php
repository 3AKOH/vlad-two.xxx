<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
    <link rel="stylesheet" href="/css/index.css<?= VER ?>">
    <link rel="stylesheet" href="/css/font-awesome.min.css<?= VER ?>">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,800" rel="stylesheet">

    <script type="text/javascript" src="/js/jquery.min.js"></script>
</head>
<body id="body">


<div id="device"></div>

<div class="login-page">
    <div class="fx login-page__fx">
        <div class="login-page__wrap">
            <div class="login-page__top">
                <div class="login-page__logo">
                    <a href="/" class="-i">
                        <img src="/img/logo.svg" class="img-responsive">
                    </a>
                    <a class="-p" href="tel:+78123261720">+7 812 326-17-20</a>
                </div>
            </div>
            <div class="login-page__center">
                <div class="login-page__www">
                    <div class="login-page__title"><span>Вход<br> в личный кабинет</span></div>
                    <div class="login-page__form">
                        <form action="/" method="post">
                            <label>
                                <span>Номер договора, 10 знаков</span>
                                <input type="text" placeholder="--------">
                            </label>
                            <label>
                                <span>Пароль</span>
                                <input type="text" placeholder="*******">
                            </label>
                            <div><button type="submit"><span>Войти</span><svg viewBox="0 0 25 7" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="1.5"><path d="M2556 567h24l-6-6" fill="none" stroke="#fff" stroke-opacity=".7" transform="translate(-2555.5 -560.5)"/></svg></button></div>
                        </form>
                    </div>
                    <div class="login-page__phone">
                        <a href="tel:+78123261720">+7 812 326-17-20</a>
                    </div>
                    <div class="login-page__links">
                        <div class="-top">
                            <div>
                                <a href="/">Как узнать номер договора?</a>
                            </div>
                            <div>
                                <a href="/">Забыли пароль?</a>
                            </div>
                        </div>
                        <div class="-bot">
                            <div>
                                <a href="/">Регистрация</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




<script src="/js/app.js"></script>


</body>
</html>
